extends TileMap

# The cells around a given cell that are considered to be neighbors
const MASK = [
	Vector2i(-1, -1),
	Vector2i(0, -1),
	Vector2i(1, -1),
	Vector2i(1, 0),
	Vector2i(1, 1),
	Vector2i(0, 1),
	Vector2i(-1, 1),
	Vector2i(-1, 0),
]
const CELL_DEAD = Vector2i(0, 0)
const CELL_LIVE = Vector2i(1, 0)

var map_size = DisplayServer.window_get_size() / 16


func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			if event.button_index == MOUSE_BUTTON_LEFT:
				# Create a live cell when the left mouse button is pressed.
				set_cell(0, event.position / 16, 0, CELL_LIVE)
			else:
				# Create a dead cell when the right mouse button is pressed.
				set_cell(0, event.position / 16, 0, CELL_DEAD)
	elif event is InputEventKey:
		# Step forward when Space is pressed.
		if event.pressed and event.keycode == KEY_SPACE:
			time_step()


# Step forward by one iteration.
func time_step() -> void:
	# Create a backup of the map so we don't overwrite it.
	var backup = []
	for i in range(map_size.x):
		backup.append([])
		for j in range(map_size.y):
			backup[i].append(get_cell_atlas_coords(0, Vector2i(i, j)))
	
	# Run the typical GoL rules on the backup map to generate the new tilemap.
	for i in range(map_size.x):
		for j in range(map_size.y):
			var pos = Vector2i(i, j)
			var neighbors = count_neighbors(backup, pos)
			if backup[i][j] == CELL_LIVE:
				if neighbors != 2 and neighbors != 3:
					set_cell(0, pos, 0, CELL_DEAD)
			elif neighbors == 3:
				set_cell(0, pos, 0, CELL_LIVE)


# Count the number of live neighbors around a cell
func count_neighbors(map: Array, pos: Vector2i) -> int:
	var count = 0
	for coord in MASK:
		var new_pos = pos + coord
		# Wrap values around to create a looping plane.
		new_pos.x = posmod(new_pos.x, map_size.x)
		new_pos.y = posmod(new_pos.y, map_size.y)
		# If a live cell is found, add 1 to the count.
		if map[new_pos.x][new_pos.y] == CELL_LIVE:
			count += 1
	return count
