extends Node2D

@onready var tile_map = $TileMap


func _ready():
	var winsize = DisplayServer.window_get_size()
	# Pre-populate the tilemap with dead cells.
	for j in range(winsize.y / 16):
		for i in range(winsize.x / 16):
			tile_map.set_cell(0, Vector2i(i, j), 0, Vector2i(0, 0))
